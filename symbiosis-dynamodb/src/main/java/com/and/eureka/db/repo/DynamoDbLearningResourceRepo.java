package com.and.eureka.db.repo;

import com.and.eureka.db.model.LearningResource;
import java.util.UUID;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

@EnableScan
public interface DynamoDbLearningResourceRepo extends
    CrudRepository<LearningResource, UUID> {

  boolean existsByTitle(String title);

}
