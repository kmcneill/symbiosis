package com.and.eureka.db.dao;

import com.and.eureka.db.model.LearningResource;
import com.and.eureka.db.repo.DynamoDbLearningResourceRepo;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class DynamoDbLearningResourceDao implements LearningResourceDao {

  private final DynamoDbLearningResourceRepo learningResourceRepo;

  @Autowired
  public DynamoDbLearningResourceDao(final DynamoDbLearningResourceRepo learningResourceRepo) {
    this.learningResourceRepo = learningResourceRepo;
  }

  @Override
  public List<LearningResource> getAll() {
    List<LearningResource> list = new ArrayList<>();
    learningResourceRepo.findAll().forEach(list::add);
    return list;
  }

  @Override
  public void create(LearningResource learningResource) {
    learningResourceRepo.save(learningResource);
  }

  @Override
  public void delete(UUID id) {
    learningResourceRepo.deleteById(id);
  }

  @Override
  public boolean exists(UUID id) {
    return learningResourceRepo.existsById(id);
  }
}
