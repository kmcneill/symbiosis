package com.and.eureka.db.dao;

import com.and.eureka.db.model.LearningResource;
import java.util.List;
import java.util.UUID;

public interface LearningResourceDao {

  List<LearningResource> getAll();

  void create(LearningResource learningResource);

  void delete(UUID id);

  boolean exists(UUID id);
}
