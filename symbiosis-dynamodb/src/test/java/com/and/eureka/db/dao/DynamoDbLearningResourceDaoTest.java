package com.and.eureka.db.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.and.eureka.db.model.LearningResource;
import com.and.eureka.db.repo.DynamoDbLearningResourceRepo;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class DynamoDbLearningResourceDaoTest {

  private DynamoDbLearningResourceDao dao;

  private DynamoDbLearningResourceRepo repo;

  @BeforeEach
  void setUp() {
    repo = mock(DynamoDbLearningResourceRepo.class);
    dao = new DynamoDbLearningResourceDao(repo);
  }

  @Test
  void testGetAll() {
    List<LearningResource> resources = Arrays.asList(learningResource("Java"),
        learningResource("Python"));
    when(repo.findAll()).thenReturn(resources);
    List<LearningResource> result = dao.getAll();

    assertEquals(result, resources);
  }

  @Test
  void testCreate() {
    LearningResource resource = learningResource("SQL");
    dao.create(resource);

    verify(repo).save(resource);
  }

  private LearningResource learningResource(String title) {
    LearningResource resource = new LearningResource();
    resource.setTitle(title);
    return resource;
  }

}