package com.and.eureka.db;

import com.amazonaws.services.dynamodbv2.local.main.ServerRunner;
import com.amazonaws.services.dynamodbv2.local.server.DynamoDBProxyServer;
import java.net.URL;
import org.apache.commons.cli.ParseException;

public class DynamoDbTestUtils {

  public static DynamoDBProxyServer createServer(String port) throws ParseException {
    URL resource = DynamoDbTestUtils.class.getClassLoader().getResource("native-libs");
    System.setProperty("sqlite4java.library.path", resource.getFile());

    return ServerRunner.createServerFromCommandLineArgs(
        new String[]{"-inMemory", "-port", port});
  }

}
