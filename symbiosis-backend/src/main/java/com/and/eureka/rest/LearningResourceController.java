package com.and.eureka.rest;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import com.and.eureka.db.dao.LearningResourceDao;
import com.and.eureka.db.model.LearningResource;
import com.and.eureka.model.DeleteLearningResourceRequest;
import com.and.eureka.openapi.LearningResourceOpenApiEndpoint;
import com.and.eureka.model.LearningResourceDto;
import java.util.List;
import java.util.UUID;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/resources")
@Validated
public class LearningResourceController implements LearningResourceOpenApiEndpoint {

  private static Logger LOGGER = LoggerFactory.getLogger(LearningResourceController.class);

  @Autowired
  private LearningResourceDao learningResourceDao;

  @GetMapping(produces = {APPLICATION_JSON_VALUE})
  @ResponseStatus(HttpStatus.OK)
  public @ResponseBody
  List<LearningResource> getAllLearningResource() {
    LOGGER.info("Call to getAllLearningResources");
    return this.learningResourceDao.getAll();
  }

  @PostMapping(consumes = {APPLICATION_JSON_VALUE}, produces = {APPLICATION_JSON_VALUE})
  @ResponseStatus(HttpStatus.CREATED)
  public ResponseEntity<String> createLearningResource(
      @RequestBody @NotNull @Valid LearningResourceDto newLearningResource) {
    LOGGER.info("Call to createLearningResource, with value: {}", newLearningResource);

    this.learningResourceDao.create(createLearningResourceEntity(newLearningResource));

    return ResponseEntity.status(HttpStatus.CREATED)
        .body("Resource created with Title: ".concat(newLearningResource.getTitle()));
  }

  @DeleteMapping(consumes = {APPLICATION_JSON_VALUE}, produces = {MediaType.TEXT_PLAIN_VALUE})
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public ResponseEntity<String> deleteLearningResource(
      @RequestBody @NotNull @Valid DeleteLearningResourceRequest request) {

    UUID resourceId = request.getId();

    if (!learningResourceDao.exists(resourceId)) {
      return ResponseEntity.status(HttpStatus.BAD_REQUEST)
          .body(
              String.format(
                  "No Learning Resource found for ID: %s", resourceId));
    }

    learningResourceDao.delete(resourceId);

    return ResponseEntity.status(HttpStatus.CREATED)
        .body(
            String.format("Resource with ID '%s' has been deleted", resourceId));
  }

  private LearningResource createLearningResourceEntity(LearningResourceDto newLearningResource) {
    LearningResource entity = new LearningResource();
    BeanUtils.copyProperties(newLearningResource, entity);
    return entity;
  }

}
