package com.and.eureka.rest.exceptionhandler;

import javax.validation.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

  private static Logger LOGGER = LoggerFactory.getLogger(RestResponseEntityExceptionHandler.class);

  @ExceptionHandler(value = {ConstraintViolationException.class})
  @ResponseStatus(value = HttpStatus.BAD_REQUEST,
      reason = "Status is returned for validation errors on input parameters or request body")
  protected ResponseEntity<Object> handleConstraintViolation(ConstraintViolationException ex,
      WebRequest request) {

    LOGGER.warn("A Validation Exception occurred, preparing a suitable response...");

    StringBuilder builder = new StringBuilder("List of Violations: ");
    ex.getConstraintViolations().forEach(v ->{
      builder.append(v.getMessage());
    });

    ResponseEntity<Object> objectResponseEntity = handleExceptionInternal(ex, ex.getConstraintViolations(),
        new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    return objectResponseEntity;
  }

}
