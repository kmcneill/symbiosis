package com.and.eureka.openapi;

import com.and.eureka.db.model.LearningResource;
import com.and.eureka.model.LearningResourceDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * Defines the (basic) Open API Contract, use in conjunction with the Spring REST annotations in the
 * implementation class.
 * <p>
 * Moving the Open API annotations to this level saves cluttering the REST Controller with a
 * confusing level of annotations.
 */
public interface LearningResourceOpenApiEndpoint {

  /* TODO Work out how to do this programmatically, so it adapts to any model changes.*/
  String LEARNING_RESOURCES_EXAMPLE =
      "[ {'id':1, 'title':'SQL', 'description':'SQL Resource', 'url':'http://sql.com/',"
          + " 'type':'development', 'category':'backend', 'tags':''} ]";

  /* TODO Find a library that has these as strings and use that directly in the method annotations instead*/
  /* Status Codes */
  String CREATED = "201";
  String OK = "200";
  String BAD_REQUEST = "400";
  String NOT_FOUND = "404";


  @Operation(summary = "Get All Learning Resources",
      description = "Returns all Learning Resources as a List", responses = {})
  @ApiResponses(value = {
      @ApiResponse(responseCode = OK, description = "Returned All Learning Resources",
          content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
              schema = @Schema(example = LEARNING_RESOURCES_EXAMPLE))}),
      @ApiResponse(responseCode = NOT_FOUND, description = "Could not find Learning Resources",
          content = @Content)})
  Iterable<LearningResource> getAllLearningResource();

  @Operation(summary = "Create Learning Resource",
      description = "Creates a new Learning Resource with the provided values")
  @ApiResponses(value = {
      @ApiResponse(responseCode = CREATED, description = "Learning Resource created",
          content = {@Content(mediaType = MediaType.TEXT_PLAIN_VALUE)}),
      @ApiResponse(responseCode = BAD_REQUEST, description = "A Validation error occurred",
          content = @Content)
  })
  ResponseEntity<String> createLearningResource(
      @RequestBody @NotNull @Valid
          LearningResourceDto newLearningResource);
}
