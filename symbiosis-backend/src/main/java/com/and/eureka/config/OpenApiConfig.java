package com.and.eureka.config;

import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenApiConfig {

  @Value("${confluence.eureka.url}")
  private String eurekaConfluencePage;

  @Bean
  public OpenAPI openAPIDocs() {
    return new OpenAPI()
        .info(
            new Info().title("Eureka REST API")
                .description("The REST API for the Eureka Learning Resources Application")
                .version("v0.1.0")
        )
        .externalDocs(
            new ExternalDocumentation()
                .description("Confluence - Eureka")
                .url(eurekaConfluencePage)
        );
  }

}
