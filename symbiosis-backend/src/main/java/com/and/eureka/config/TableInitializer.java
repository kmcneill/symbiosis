package com.and.eureka.config;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.and.eureka.db.model.LearningResource;
import java.util.Arrays;
import java.util.List;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("local")
public class TableInitializer {

  private static Logger LOGGER = LoggerFactory.getLogger(TableInitializer.class);

  @Autowired
  private DynamoDBMapper dynamoDBMapper;

  @Autowired
  private AmazonDynamoDB amazonDynamoDB;

  @PostConstruct
  public void createTables() {

    List<String> existingTables = amazonDynamoDB.listTables().getTableNames();

    for (Class<?> clazz : Arrays.asList(LearningResource.class)) {
      String proposedClassName = clazz.getSimpleName();

      LOGGER.info("Attempting to create table: " + proposedClassName);
      if (!existingTables.contains(proposedClassName)) {
        CreateTableRequest tableRequest = dynamoDBMapper
            .generateCreateTableRequest(clazz);
        tableRequest.setProvisionedThroughput(
            new ProvisionedThroughput(1L, 1L));
        amazonDynamoDB.createTable(tableRequest);
        LOGGER.info(" -- Table created");
      } else {
        LOGGER.info(" -- Table already exists");
      }

    }
  }

}
