package com.and.eureka.model;

import java.net.URL;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LearningResourceDto {

  @NotNull(message = "'title' cannot be null")
  @Size(min = 1, max = 50)
  private String title;

  @NotNull(message = "'description' cannot be null")
  @Size(min = 1, max = 256)
  private String description;

  @NotNull(message = "'url' cannot be null")
  private URL url;

  @NotNull(message = "'type' cannot be null")
  @Size(min = 1, max = 50)
  private String type;

  @NotNull(message = "'category' cannot be null")
  @Size(min = 1, max = 50)
  private String category;

  private String tags;

}
