package com.and.eureka;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;

@SpringBootTest
@Profile("test")
class EurekaApplicationTest {

    @Test
    void contextLoads() {
    }

}
