package com.and.eureka.db;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.local.main.ServerRunner;
import com.amazonaws.services.dynamodbv2.local.server.DynamoDBProxyServer;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.and.eureka.db.model.LearningResource;
import com.and.eureka.db.repo.DynamoDbLearningResourceRepo;
import java.net.URL;
import org.apache.commons.cli.ParseException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;

public class DynamoDbIntegrationTest {

  public static final String TABLE_LEARNING_RESOURCE = "LearningResource";
  private static final String DYNAMO_DB_SERVER_PORT = "8081";

  private static DynamoDBProxyServer server;

  @Autowired
  private DynamoDBMapper dynamoDBMapper;

  @Autowired
  private AmazonDynamoDB amazonDynamoDB;

  @Autowired
  protected DynamoDbLearningResourceRepo repo;

  @BeforeAll
  static void startServer() throws Exception {
    server = createServer();
    server.start();
  }

  @AfterAll
  static void teardownClass() throws Exception {
    server.stop();
  }

  @BeforeEach
  void setup() {
    amazonDynamoDB.createTable(createTableRequest(LearningResource.class));
  }

  @AfterEach
  void teardown() {
    amazonDynamoDB.deleteTable(TABLE_LEARNING_RESOURCE);
  }

  protected CreateTableRequest createTableRequest(Class<?> clazz) {
    CreateTableRequest tableRequest = dynamoDBMapper
        .generateCreateTableRequest(clazz);
    tableRequest.setProvisionedThroughput(
        new ProvisionedThroughput(1L, 1L));
    return tableRequest;
  }

  private static DynamoDBProxyServer createServer() throws ParseException {
    URL resource = DynamoDbIntegrationTest.class.getClassLoader().getResource("native-libs");
    System.setProperty("sqlite4java.library.path", resource.getFile());

    server = ServerRunner.createServerFromCommandLineArgs(
        new String[]{"-inMemory", "-port", DYNAMO_DB_SERVER_PORT});

    return server;
  }

}
