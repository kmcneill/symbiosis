package com.and.eureka.rest;

import static java.lang.String.format;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.and.eureka.db.DynamoDbIntegrationTest;
import com.and.eureka.db.model.LearningResource;
import com.and.eureka.model.DeleteLearningResourceRequest;
import com.and.eureka.model.LearningResourceDto;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;
import org.apache.commons.beanutils.PropertyUtils;
import org.assertj.core.util.Streams;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles({"test"})
class LearningResourceControllerTest extends DynamoDbIntegrationTest {

  private static final String ENDPOINT = "/api/resources";

  @Autowired
  private MockMvc mvc;

  private ObjectMapper objectMapper = new ObjectMapper();

  private LearningResource sampleLearningResource;

  @Test
  void testCreateResource() throws Exception {
    LearningResourceDto newResource = createLearningResourceDto("SQL");
    String resourceAsJson = objectMapper.writeValueAsString(newResource);

    mvc.perform(post(ENDPOINT)
            .content(resourceAsJson)
            .contentType(MediaType.APPLICATION_JSON))
        .andDo(MockMvcResultHandlers.print())
        .andExpect(status().isCreated());

    Optional<LearningResource> found = Streams.stream(repo.findAll())
        .filter(resource -> newResource.getTitle()
            .equals(resource.getTitle()))
        .findFirst();

    assertTrue(found.isPresent());
  }

  @Test
  void testGetAllResources() throws Exception {
    sampleLearningResource = createLearningResourceEntity("Javascript");
    repo.save(sampleLearningResource);

    MockHttpServletResponse response = mvc.perform(get(ENDPOINT)
            .accept(MediaType.APPLICATION_JSON))
        .andDo(MockMvcResultHandlers.print())
        .andExpect(status().isOk())
        .andReturn().getResponse();

    List<LearningResource> result = objectMapper.readValue(response.getContentAsString(),
        new TypeReference<>() {
        });

    assertThat(result.get(0), equalTo(sampleLearningResource));
  }

  @Test
  void testDeleteResource_Success() throws Exception {
    String title = "Javascript";

    LearningResource savedResource = saveLearningResource(title);

    MockHttpServletResponse response = mvc.perform(delete(ENDPOINT)
        .content(objectMapper.writeValueAsString(savedResource))
        .contentType(MediaType.APPLICATION_JSON)
    ).andExpect(status().is2xxSuccessful()).andReturn().getResponse();

    assertFalse(repo.existsByTitle(title));

    assertThat(response.getContentAsString(),
        containsString(format("Resource with ID '%s' has been deleted", savedResource.getId())));
  }

  private LearningResource saveLearningResource(String title) throws MalformedURLException {
    sampleLearningResource = createLearningResourceEntity(title);
    LearningResource savedResource = repo.save(sampleLearningResource);
    assertTrue(repo.existsByTitle(title));
    return savedResource;
  }

  @Test
  void testDeleteResource_NotFound() throws Exception {
    UUID id = UUID.randomUUID();
    String json = objectMapper.writeValueAsString(
        new DeleteLearningResourceRequest(id));

    MockHttpServletResponse response = mvc.perform(delete(ENDPOINT)
        .content(json)
        .contentType(MediaType.APPLICATION_JSON)
    ).andExpect(status().isBadRequest()).andReturn().getResponse();

    assertThat(response.getContentAsString(),
        containsString(format("No Learning Resource found for ID: %s", id)));
  }

  @Test
  void testDeleteResource_NoRequestBody() throws Exception {
    testDeleteResource_ValidationError("",
        ".*Required request body is missing.*");
  }

  @Test
  void testDeleteResource_MissingResourceID() throws Exception {
    String json = objectMapper.writeValueAsString(
        new DeleteLearningResourceRequest());

    testDeleteResource_ValidationError(json,
        ".*Field error in object 'deleteLearningResourceRequest' on field 'id'"
            .concat(".*must not be null.*"));
  }

  private void testDeleteResource_ValidationError(String json, String msg) throws Exception {
    MvcResult result = mvc.perform(delete(ENDPOINT)
        .content(json)
        .contentType(MediaType.APPLICATION_JSON)
    ).andExpect(status().isBadRequest()).andReturn();

    assertThat(result.getResolvedException().getMessage(),
        Matchers.matchesPattern(msg));
  }

  @ParameterizedTest
  @ValueSource(strings = {"title", "description", "url", "type", "category"})
  void testNotNullFieldValidation(String fieldName) throws Exception {
    LearningResourceDto newResource = createLearningResourceDto("SQL");

    PropertyUtils.setSimpleProperty(newResource, fieldName, null);

    testCreateResource_withNullValidationError(newResource, fieldName);
  }

  @ParameterizedTest
  @MethodSource("sizeValidationValues")
  void testSizeFieldValidation(String fieldName, int min, int max, String inputValue)
      throws Exception {
    LearningResourceDto newResource = createLearningResourceDto("SQL");

    PropertyUtils.setSimpleProperty(newResource, fieldName, inputValue);

    testCreateResource_withSizeValidationError(newResource, fieldName, min, max);
  }

  private static Stream<Arguments> sizeValidationValues() {
    return Stream.of(
        Arguments.of("title", 1, 50, ""),
        Arguments.of("description", 1, 256, ""),
        Arguments.of("type", 1, 50, ""),
        Arguments.of("category", 1, 50, "")
    );
  }

  void testCreateResource_withNullValidationError(LearningResourceDto newResource,
      String nullFieldName) throws Exception {

    this.testCreateResource_withValidationError(newResource, nullFieldName, "cannot be null");
  }

  void testCreateResource_withSizeValidationError(LearningResourceDto newResource,
      String fieldName, int min, int max) throws Exception {

    this.testCreateResource_withValidationError(newResource, fieldName,
        "size must be between " + min + " and " + max);
  }

  void testCreateResource_withValidationError(LearningResourceDto newResource, String fieldName,
      String message) throws Exception {
    String resourceAsJson = objectMapper.writeValueAsString(newResource);

    MvcResult result = mvc.perform(post(ENDPOINT)
            .content(resourceAsJson)
            .contentType(MediaType.APPLICATION_JSON))
        .andDo(MockMvcResultHandlers.print())
        .andExpect(status().isBadRequest())
        .andReturn();

    assertEquals(0, repo.count());

    assertThat(result.getResolvedException().getMessage(),
        Matchers.matchesPattern(
            "Validation failed for argument.*'" + fieldName + "'.*" + message + ".*"));
  }


  private static LearningResourceDto createLearningResourceDto(String title)
      throws MalformedURLException {
    URL url = new URL("https://".concat(title.toLowerCase()).concat(".com/"));
    return new LearningResourceDto(title, title.concat(" Resource"), url,
        title.concat(" Type"), title.concat(" category"), "");
  }

  private LearningResource createLearningResourceEntity(String title)
      throws MalformedURLException {
    URL url = new URL("https://".concat(title.toLowerCase()).concat(".com/"));
    return new LearningResource(null, title,
        title + " Resource", url, title + " Type", title + " category", null);
  }

}
