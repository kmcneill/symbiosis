import React from 'react';
import '../App.css';
import AppNavbar from '../components/AppNavbar';
import CreateResourceForm from '../components/CreateResourceForm';
import { Container, Col } from 'reactstrap';


const AddResource = () => {
    return (
      <div>
        <AppNavbar />
        <Container >
          <Col>
            <div className="col-md-8 offset-md-2 col-xs-10 offset-xs-1">
              <CreateResourceForm />
            </div>
          </Col>
        </Container>
      </div>
    );
}

export default AddResource;
