import React from 'react';
import '../App.css';
import AppNavbar from '../components/AppNavbar';
import {Col, Container, Row} from 'reactstrap';

const Profile = () => {

  return (
    <div>
      <AppNavbar />
        <Container>
          <Row>
              <Col>
                <img src='./rodney.png' height='300' title='Listen to Rodney'
                     alt='rodney cartoon'/>
              </Col>
          </Row>
        </Container>
    </div>
  );
}

export default Profile;

