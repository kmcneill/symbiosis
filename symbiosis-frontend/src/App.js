import React, { Component } from 'react';
import './App.css';
import Home from './pages/Home';
import Profile from './pages/Profile';
import AddResource from './pages/AddResource';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

class App extends Component {
  render() {
    return (
        <Router>
          <Switch>
            <Route path='/' exact={true}>
                <Home/>
            </Route>
            <Route path='/home'>
                <Home/>
            </Route>
            <Route path='/add'>
              <AddResource/>
            </Route>
            <Route path='/profile'>
                <Profile/>
            </Route>
          </Switch>
        </Router>
    )
  }
}

export default App;