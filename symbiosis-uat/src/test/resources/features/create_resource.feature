Feature: Create a new Learning Resource

  Scenario Outline: Successfully Creates a Learning Resource
    Given I wish to add a new learning resoure
    And I enter the <title> <description> <url> <type> <category> and <tags> in the Create Resource form
    When I submit the form
    Then The resource will be saved in the database with title equals <title>
    And The user will be informed the action was successful
    Examples:
      | title        | description      | url                     | type       | category   | tags                 |
      | "SQL"        | "About SQL"      | "https://sql.com/"        | "database" | "backend"  | ""                   |
      | "Javascript" | "JS Description" | "https://javascript.org/" | "code"     | "frontend" | "mustsee, essential" |
