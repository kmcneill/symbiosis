@wip @read_resource
Feature: Read a learning resource

    Scenario Outline: Successfully displays a learning resource
        	
        Given I am on the Home Page
        When I select a particular resource to view
        Then I am able to view the following details of a resource - Title, Description, Category, Type, URL 
        Examples:
            | id           | title        | description      | url                     | type       | category   | tags                 |
            | 1            | "Javascript" | "JS Description" | "https://javascript.org/" | "code"     | "frontend" | "mustsee, essential" |

    Scenario Outline: Successfully close a displayed learning resource
       	Given I am viewing a resource
        When When I exit the resource
        Then I am returned to the Home Page