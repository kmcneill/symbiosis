@wip
Feature: View All Learning Resources

  Scenario: Successfully Fetch All Learning Resources
    Given I have the following Resources in the Database:
      | title      | description    | url                     | type     | category | tags               |
      | SQL        | About SQL      | https://sql.com/        | database | backend  |                    |
      | Javascript | JS Description | https://javascript.org/ | code     | frontend | mustsee, essential |
    When When I view the home screen
    Then I should see the following list of resources on my screen:
      | title      | type     | category |
      | SQL        | database | backend  |
      | Javascript | code     | frontend |