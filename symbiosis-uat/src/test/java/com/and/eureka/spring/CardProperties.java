package com.and.eureka.spring;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "resources.card.css-selectors")
@Getter
@Setter
public class CardProperties {

  private String titleSelector;

  private String typeSelector;

  private String categorySelector;

}
