package com.and.eureka.spring;


import com.amazonaws.services.dynamodbv2.local.main.ServerRunner;
import com.amazonaws.services.dynamodbv2.local.server.DynamoDBProxyServer;
import com.and.eureka.db.DynamoDBConfig;
import com.and.eureka.db.DynamoDbTestUtils;
import com.and.eureka.os.OsFinder;
import io.github.bonigarcia.wdm.WebDriverManager;
import java.net.URL;
import java.time.Duration;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.apache.commons.cli.ParseException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.FluentWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@EnableConfigurationProperties({FormProperties.class, CardProperties.class})
@ComponentScan("com.and.eureka")
@Import(DynamoDBConfig.class)
public class SpringBootAppConfig {

  private DynamoDBProxyServer server;

  @Autowired
  public SpringBootAppConfig(@Value("${amazon.dynamodb.port}") String dynamoDbServerPort)
      throws Exception {
    this.server = DynamoDbTestUtils.createServer(dynamoDbServerPort);
  }

  @PostConstruct
  public void startServer() throws Exception {
    server.start();
  }

  @PreDestroy
  public void stopServer() throws Exception {
    server.stop();
  }

  @Bean
  public WebDriver webDriver() {
    WebDriverManager.chromedriver()
        .version("92")
        .operatingSystem(OsFinder.OPERATING_SYSTEM).setup();
    ChromeOptions options = new ChromeOptions();
    options.addArguments("--headless");
    options.addArguments("--allow-insecure-localhost");
    return new ChromeDriver(options);
  }

  @Bean
  public FluentWait<WebDriver> fluentWait(WebDriver driver) {
    return new FluentWait<>(driver)
        .withTimeout(Duration.ofSeconds(10))
        .pollingEvery(Duration.ofSeconds(1))
        .ignoring(NoSuchElementException.class);
  }

}
