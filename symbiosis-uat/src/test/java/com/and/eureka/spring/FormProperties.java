package com.and.eureka.spring;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "form.create-resource")
@Getter
@Setter
public class FormProperties {

    private String id;

    private String titleId;

    private String descriptionId;

    private String urlId;

    private String typeId;

    private String categoryId;

    private String tagsId;
}
