package com.and.eureka.steps;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.and.eureka.db.repo.DynamoDbLearningResourceRepo; 
import com.and.eureka.spring.FormProperties;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Wait;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;


public class ReadAResourceSteps {


  @Value("${server.url}")
  private String url;

  @Value("${modal.read-resource.title-id}")
  private String title;

  @Value("${page.title}")
  private String pageTitle;

  @Value("${modal.read-resource.description-id}")
  private String description;

  @Value("${modal.read-resource.category-id}")
  private String category;

  @Value("${modal.read-resource.url-id}")
  private String urlId;

  @Value("${modal.read-resource.type-id}")
  private String type;

  @Value("${modal.read-resource.card-id}")
  private String cardId;

  @Autowired
  private DynamoDbLearningResourceRepo repo;

  @Autowired
  private WebDriver driver;

  @Autowired
  private FormProperties formProperties;

  @Autowired
  private Wait<WebDriver> fluentWait;
    
  @Before
  public void setUp() {
    driver.get(url);
  }

  @After
  public void cleanUp() {
    repo.deleteAll();
  }

  @Given("I am on the Home Page")
  public void i_am_on_the_home_page() {

    assertEquals(pageTitle, driver.getTitle());
  
  }

  @When("I select a particular resource to view")
  public void i_select_a_particular_resource_to_view(int id, String title, String description,
  String url, String type, String category, String tags) {

    WebElement card = fluentWait.until(
      ExpectedConditions.visibilityOfElementLocated(By.id(formProperties.getId()))
  );

  }

  @Then("I am able to view the following details of a resource - Title, Description, Category, Type, URL")
  public void i_am_able_to_view_the_following_details_of_a_resource_Title_Description_Category_Type_URL() {

    assertEquals(pageTitle, driver.getTitle());

  }

}


