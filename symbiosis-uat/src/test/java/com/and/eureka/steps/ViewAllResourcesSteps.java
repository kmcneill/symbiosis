package com.and.eureka.steps;

import com.and.eureka.components.ResourceSummaryCard;
import com.and.eureka.db.model.LearningResource;
import com.and.eureka.db.repo.DynamoDbLearningResourceRepo;
import com.and.eureka.spring.CardProperties;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import java.util.Arrays;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.lang.String.format;
import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ViewAllResourcesSteps {

    public static final String TITLE = "title";

    @Value("${server.url}")
    private String url;

    @Value("${page.title}")
    private String pageTitle;

    @Value("${resources.card.class}")
    private String resourceCardSelector;

    @Autowired
    private DynamoDbLearningResourceRepo repo;

    @Autowired
    private WebDriver driver;

    @Autowired
    private CardProperties cardProperties;

    @Before
    public void setUp() {
        driver.get(url);
    }

    @Given("I have the following Resources in the Database:")
    public void i_have_the_following_Resources_in_the_Database(DataTable dataTable) throws MalformedURLException {

        List<Map<String, String>> resourceRows = dataTable.asMaps();

        for (Map<String, String> row : resourceRows) {
            repo.save(createLearningResource(row));
        }
        // Don't move to the next step till the DB action is complete.
        await()
                .until(() -> repo.count() == resourceRows.size());
    }

    @When("When I view the home screen")
    public void whenIViewTheHomeScreen() {
        assertEquals(pageTitle, driver.getTitle());
    }

    @Then("I should see the following list of resources on my screen:")
    public void seeResourceList(DataTable dataTable) {
        List<Map<String, String>> maps = dataTable.asMaps();

        List<WebElement> elements = driver.findElements(By.className(resourceCardSelector));

        assertNumberOfCardsFound(maps, elements);

        for (Map<String, String> map : maps) {
            WebElement elem = findAssociatedCard(map, elements);
            ResourceSummaryCard card = new ResourceSummaryCard(elem, cardProperties);
            compareCardValues(map, card);
        }
    }

    private void compareCardValues(Map<String, String> rowExpected, ResourceSummaryCard card) {
        assertCardValue(rowExpected.get("title"), card.getTitle());
        assertCardValue(rowExpected.get("type"), card.getType());
        assertCardValue(rowExpected.get("category"), card.getCategory());
    }

    private void assertCardValue(String expected, String actual) {
        assertEquals(expected, actual,
                format("Expected value '%s' for type but was '%s'",
                        expected, actual));
    }

    private void assertNumberOfCardsFound(List<Map<String, String>> maps, List<WebElement> elements) {
        int expected = maps.size();
        assertEquals(expected, elements.size(),
                format("Expected %d Learning Resources but found %d",
                        expected, elements.size()));
    }

    private WebElement findAssociatedCard(Map<String, String> rowMap, List<WebElement> elements) {
        String expectedTitle = rowMap.get(TITLE);

        Optional<WebElement> card = elements.stream().filter(elem -> {
            WebElement found = elem.findElement(By.cssSelector(cardProperties.getTitleSelector()));
            return expectedTitle.equals(found.getText());
        }).findFirst();

        assertTrue(card.isPresent(),
                format("Could not find resource where element with name 'title' had value '%s'",
                        expectedTitle));

        return card.get();
    }

    private LearningResource createLearningResource(Map<String, String> map) throws MalformedURLException {
        LearningResource resource = new LearningResource();
        resource.setTitle(map.get(TITLE));
        resource.setDescription(map.get("description"));
        resource.setUrl(new URL(map.get("url")));
        resource.setType(map.get("type"));
        resource.setCategory(map.get("category"));
        resource.setTags(createTagsList(map));
        return resource;
    }

    private List<String> createTagsList(Map<String, String> map){
        return Arrays.asList(map.get("tags").split(","));
    }

}
