package com.and.eureka.steps;

import static org.awaitility.Awaitility.await;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.and.eureka.components.CreateResourceForm;
import com.and.eureka.db.repo.DynamoDbLearningResourceRepo;
import com.and.eureka.spring.FormProperties;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

public class CreateResourceSteps {

  @Value("${server.url}")
  private String url;

  @Value("${page.title}")
  private String pageTitle;

  @Value("${navbar.add-resource-button.id}")
  private String addResourceButtonId;

  @Value("${user-feedback.id}")
  private String userFeedbackId;

  @Value("${user-feedback.resource-created-msg}")
  private String resourceCreatedMsg;

  @Autowired
  private DynamoDbLearningResourceRepo repo;

  @Autowired
  private FormProperties formProperties;

  @Autowired
  private WebDriver driver;

  @Autowired
  private Wait<WebDriver> fluentWait;

  private CreateResourceForm createResourceForm;

  private String newResourceTitle;

  @Before
  public void setUp() {
    driver.get(url);
  }

  @Given("I wish to add a new learning resoure")
  public void i_wish_to_add_a_new_learning_resoure() {
    assertEquals(pageTitle, driver.getTitle());
    driver.findElement(By.id(addResourceButtonId))
        .click();
  }

  @Given("I enter the {string} {string} {string} {string} {string} and {string} in the Create Resource form")
  public void i_enter_the_and_in_the_Create_Resource_form(String title, String description,
      String url, String type, String category, String tags) {
    this.newResourceTitle = title;

    WebElement formElement = fluentWait.until(
        ExpectedConditions.visibilityOfElementLocated(By.id(formProperties.getId()))
    );

    createResourceForm = new CreateResourceForm(formElement, formProperties);

    this.createResourceForm
        .setTitle(title).setDescription(description)
        .setUrl(url).setType(type)
        .setCategory(category)
        // .setTags(tags) TODO Add this back when tags story is ready
    ;
  }

  @When("I submit the form")
  public void i_submit_the_form() {
    this.createResourceForm.submit();
  }

  @Then("The resource will be saved in the database with title equals {string}")

  public void the_the_resource_will_be_saved_in(String title) {
    await().until(() -> learningResourceFound(title));

    assertTrue(learningResourceFound(title),
        "Could not find learning resource with title = " + title);
  }

  private boolean learningResourceFound(String title) {
    return repo.existsByTitle(title);
  }

  @Then("The user will be informed the action was successful")
  public void the_user_will_be_informed_the_action_was_successful() {
    WebElement feedbackBlock = fluentWait.until(
        ExpectedConditions.visibilityOfElementLocated(By.id(userFeedbackId))
    );

    String expectedSuccessMessage = String.format(resourceCreatedMsg, newResourceTitle);

    assertThat(feedbackBlock.getText(),
        containsString(expectedSuccessMessage));
  }

}
