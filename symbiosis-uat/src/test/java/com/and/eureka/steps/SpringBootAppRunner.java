package com.and.eureka.steps;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.and.eureka.EurekaApplication;
import com.and.eureka.db.model.LearningResource;
import com.and.eureka.db.repo.DynamoDbLearningResourceRepo;
import com.and.eureka.spring.SpringBootAppConfig;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.spring.CucumberContextConfiguration;
import java.util.concurrent.TimeUnit;
import org.awaitility.Awaitility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@CucumberContextConfiguration
@ActiveProfiles("uat")
@SpringBootTest(classes = {EurekaApplication.class,
    SpringBootAppConfig.class}, webEnvironment = DEFINED_PORT)
public class SpringBootAppRunner {

  public static final String TABLE_LEARNING_RESOURCE = "LearningResource";

  @Autowired
  private DynamoDBMapper dynamoDBMapper;

  @Autowired
  private AmazonDynamoDB amazonDynamoDB;

  @Autowired
  private DynamoDbLearningResourceRepo repo;

  @Before
  public void setUp() {
    if (!amazonDynamoDB.listTables().getTableNames().contains(TABLE_LEARNING_RESOURCE)) {
      amazonDynamoDB.createTable(createTableRequest(LearningResource.class));
    }

    Awaitility.setDefaultPollInterval(2, TimeUnit.SECONDS);
    Awaitility.setDefaultTimeout(10, TimeUnit.SECONDS);
  }

  @After
  public void cleanUp() {
    repo.deleteAll();
  }

  protected CreateTableRequest createTableRequest(Class<?> clazz) {
    CreateTableRequest tableRequest = dynamoDBMapper
        .generateCreateTableRequest(clazz);
    tableRequest.setProvisionedThroughput(
        new ProvisionedThroughput(1L, 1L));
    return tableRequest;
  }

}
