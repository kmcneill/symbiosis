package com.and.eureka;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)
@CucumberOptions(features = "classpath:features",
        glue = "com.and.eureka.steps",
        plugin = {"pretty", "html:target/cucumber-reports"},
        tags = {"@read_resource"})
public class CucumberIT {

}
