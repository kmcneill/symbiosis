package com.and.eureka.components;

import com.and.eureka.spring.CardProperties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class ResourceSummaryCard {

    private final WebElement cardElement;

    private final CardProperties cardProperties;

    public ResourceSummaryCard(WebElement cardElement, CardProperties cardProperties) {
        this.cardElement = cardElement;
        this.cardProperties = cardProperties;
    }

    public String getType() {
        return cardElement.findElement(By.cssSelector(cardProperties.getTypeSelector())).getText();
    }

    public String getCategory() {
        return cardElement.findElement(By.cssSelector(cardProperties.getCategorySelector())).getText();
    }

    public String getTitle() {
        return cardElement.findElement(By.cssSelector(cardProperties.getTitleSelector())).getText();
    }
}
