package com.and.eureka.components;

import com.and.eureka.spring.FormProperties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class CreateResourceForm {

    private final WebElement form;

    private FormProperties formProperties;

    public CreateResourceForm(WebElement form, FormProperties formProperties) {
        this.form = form;
        this.formProperties = formProperties;
    }

    public CreateResourceForm setTitle(String title) {
        this.form.findElement(By.id(formProperties.getTitleId())).sendKeys(title);
        return this;
    }

    public CreateResourceForm setDescription(String description) {
        this.form.findElement(By.id(formProperties.getDescriptionId())).sendKeys(description);
        return this;
    }

    public CreateResourceForm setUrl(String url) {
        this.form.findElement(By.id(formProperties.getUrlId())).sendKeys(url);
        return this;
    }

    public CreateResourceForm setType(String type) {
        this.form.findElement(By.id(formProperties.getTypeId())).sendKeys(type);
        return this;
    }

    public CreateResourceForm setCategory(String category) {
        this.form.findElement(By.id(formProperties.getCategoryId())).sendKeys(category);
        return this;
    }

    public CreateResourceForm setTags(String tags) {
        this.form.findElement(By.id(formProperties.getTagsId())).sendKeys(tags);
        return this;
    }

    public void submit() {
        this.form.submit();
    }
}
